/*1.Взять CSS генератор который мы писали на уроке и добавить кнопку,
при нажатии на которую мы будем выводить весь результат наших inputs в какой то div
в виде стиля css соответствующий стилю border radius.*/

const rangeTl = document.getElementById('tlr'),
    rangeTr = document.getElementById('trr'),
    rangeBl = document.getElementById('blr'),
    rangeBr = document.getElementById('brr');

const resultTl = document.getElementById('result-tlr'),
    resultTr = document.getElementById('result-trr'),
    resultBl = document.getElementById('result-blr'),
    resultBr = document.getElementById('result-brr');

const inputs = document.querySelectorAll('.input');
const res = document.getElementsByClassName('.res');
const span = document.getElementById('totalResult');
const cube = document.getElementById('cube');
const btn = document.getElementById('btn');

function changeRadius() {
    resultTl.innerHTML = rangeTl.value;
    resultTr.innerHTML = rangeTr.value;
    resultBl.innerHTML = rangeBl.value;
    resultBr.innerHTML = rangeBr.value;

    cube.style.borderRadius = rangeTl.value + 'px ' + rangeTr.value + 'px ' + rangeBr.value + 'px ' + rangeBl.value + 'px ';
}

btn.addEventListener('click', function () {
    res.innerHTML = rangeTl.value + ' px ' + rangeTr.value + ' px ' + rangeBl.value + ' px ' + rangeBr.value+ ' px ';
    span.innerHTML = res.innerHTML;
})
for (input of inputs) {
    input.addEventListener('input', changeRadius);
}